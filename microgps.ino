#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define RXPIN 3
#define TXPIN 2
#define OLED_RESET 4

String info = "MicroGPS v0.1";

Adafruit_SSD1306 display(OLED_RESET);

TinyGPSPlus gps;

SoftwareSerial nss(RXPIN, TXPIN);

void setup() {
  nss.begin(9600);
  
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  updateDisplay(info, 0, 0, false);
}

void loop() {
  while (nss.available() > 0) {
    if (gps.encode(nss.read())) { 
         if (gps.location.isValid()) {
          String text  = info + "   [" + String(gps.satellites.value()) + "]" +
          "\nAlt:" + String(gps.altitude.meters(), 2) + "m" +
          "\nLat:" + (gps.location.rawLat().negative ? "-" : "+") + String(gps.location.rawLat().deg) + "." +String(gps.location.rawLat().billionths) +
          "\nLng:" +  (gps.location.rawLng().negative ? "-" : "+") + String(gps.location.rawLng().deg) + "." +String(gps.location.rawLng().billionths);
          updateDisplay(text, 0, 0, false);  
        } 
    }
    // NO GPS Data Detected
    if (millis() > 5000 && gps.charsProcessed() < 10) {
      updateDisplay("No GPS Detected", 0, 0, true);
      while(true);
    }
  }
}

void updateDisplay(String text, int right, int top, boolean scroll) { //TODO Take an array for each line
  if (!scroll) {
    display.stopscroll();
  }
  display.setTextSize(.04);
  display.setTextColor(WHITE);
  display.setCursor(right, top);
  display.clearDisplay();
  display.println(text);
  display.display();
  if (scroll) {
    display.startscrollright(0x00, 0x0F);
  } 
}
